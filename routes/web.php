<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function()
{
    return view('home');
})->name('home');

Route::prefix('register')->group(function () {
    Route::get('/', function() {return view('register');})->name('register');
    Route::post('/submit','UsersController@addUser')->name('register.form');
});

Route::prefix('users')->group(function () {
    Route::get('/','UsersController@allUsers')->name('users');
    Route::prefix('/{id}')->group(function () {
        Route::get('/', 'UsersController@showOneUser')->name('one.user');
        Route::get('/edit', 'UsersController@editUser')->name('edit.user');
        Route::put('/', 'UsersController@saveUsersUpdating')->name('save.user.updating');
        Route::delete('/', 'UsersController@deleteUser')->name('delete.user');

        Route::prefix('/phones')->group(function () {
            Route::get('/','PhonesController@allPhones')->name('phones.list');
            Route::get('/edit','PhonesController@editPhone')->name('edit.phone');
            Route::put('/','PhonesController@savePhoneUpdating')->name('save.phone.updating');
            Route::delete('/','PhonesController@deletePhone')->name('delete.phone');
        });
    });
});

Route::prefix('phone')->group(function () {
    Route::get('/add','PhonesController@add')->name('add.phone');
    Route::post('/','PhonesController@create')->name('create.phone');
});



