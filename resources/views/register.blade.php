@extends('layouts.app')

@section('title') Registration @endsection

@section('content')

    <h1>Register</h1>
    <form action="{{ route('register.form') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="firstname">First Name</label>
            <input type="text" name="firstname" placeholder="First Name" id="firstname" class="form-control">
        </div>
        <div class="form-group">
            <label for="lastname">Last Name</label>
            <input type="text" name="lastname" placeholder="Last Name" id="lastname" class="form-control">
        </div>
        <div class="form-group">
        <label for="email">Email</label>
            <input type="text" name="email" placeholder="Email" id="email" class="form-control">
        </div>
        <div class="form-group">
        <label for="country">Country</label>
            <input type="text" name="country" placeholder="Country" id="country" class="form-control">
        </div>
        <div class="form-group">
        <label for="city">City</label>
            <input type="text" name="city" placeholder="City" id="city" class="form-control">
        </div>
        <button type="submit" class="btn btn-success">Sign up</button>
    </form>
@endsection
