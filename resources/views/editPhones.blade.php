@extends('layouts.app')

@section('title') updating phones list: {{$phones->id}} @endsection

@section('content')
    <h1>Update phones list</h1>
    <form action="{{ route('save.phone.updating', $phones->id) }}" method="post">
        @csrf
        <div class="form-group">
            <label for="firstname">User ID</label>
            <input
                type="text" name="id" placeholder="User ID" id="id"
                class="form-control" value="{{ $phones->users_id }}"
            >
        </div>
        <div class="form-group">
            <label for="lastname">Model</label>
            <input
                type="text" name="model" placeholder="Phone model" id="model"
                class="form-control" value="{{ $phones->model }}"
            >
        </div>
        <div>
            <form action="{{ route('save.phone.updating', $phones->id) }}" method="put">
                <input class="btn btn-success" type="submit" value="Save" />
                @method('put')
                @csrf
            </form>
            <a href="{{ route('phones.list', $phones->id) }}" class="btn btn-secondary">Cancel</a>
        </div>
    </form>
@endsection
