@extends('layouts.app')

@section('title') Show Phones @endsection

@section('content')
    <h1>Phones Page</h1>
    <table border="1px solid black">
        <tr>
            <th>
                ID
            </th>
            <th>
                User ID
            </th>
            <th>
                Model
            </th>
            <th>
                Edit
            </th>
            <th>
                Delete
            </th>
        </tr>
        @foreach($phonesdata as $phone)
            <tr>
                <td>
                    {{ $phone->id }}
                </td>
                <td>
                    {{ $phone->users_id }}
                </td>
                <td>
                    {{ $phone->model }}
                </td>
                <td>
                    <form action="{{ route('edit.phone', $phone->id) }}" method="post">
                        <input class="btn btn-warning" type="submit" value="Edit" />
                        @method('get')
                        @csrf
                    </form>
                </td>
                <td>
                    <form action="{{ route('delete.phone', $phone->id) }}" method="post">
                        <input class="btn btn-danger" type="submit" value="Delete" />
                        @method('delete')
                        @csrf
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
