@extends('layouts.app')

@section('title') updating user: {{$usersData->id}} @endsection

@section('content')
    <h1>Update info</h1>
    <form action="{{ route('save.user.updating', $usersData->id) }}" method="post">
        @csrf
        <div class="form-group">
            <label for="firstname">First Name</label>
            <input
                type="text" name="firstname" placeholder="First Name" id="firstname"
                class="form-control" value="{{ $usersData->firstname }}"
            >
        </div>
        <div class="form-group">
            <label for="lastname">Last Name</label>
            <input
                type="text" name="lastname" placeholder="Last Name" id="lastname"
                class="form-control" value="{{ $usersData->lastname }}"
            >
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input
                type="text" name="email" placeholder="Email" id="email"
                class="form-control" value="{{ $usersData->email }}"
            >
        </div>
        <div class="form-group">
            <label for="country">Country</label>
            <input
                type="text" name="country" placeholder="Country" id="country"
                class="form-control" value="{{ $usersData->country }}"
            >
        </div>
        <div class="form-group">
            <label for="city">City</label>
            <input
                type="text" name="city" placeholder="City" id="city"
                class="form-control" value="{{ $usersData->city }}"
            >
        </div>
        <div>
            <form action="{{ route('save.user.updating', $usersData->id) }}" method="put">
                <input class="btn btn-success" type="submit" value="Save" />
                @method('put')
                @csrf
            </form>
            <a href="{{ route('one.user', $usersData->id) }}" class="btn btn-secondary">Cancel</a>
        </div>
    </form>
@endsection
