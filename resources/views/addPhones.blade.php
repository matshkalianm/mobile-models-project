@extends('layouts.app')

@section('title')Add Phones @endsection
@section('content')

    <h1>Add Phones</h1>
    <form action="{{ route('create.phone') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="firstname">User ID</label>
            <input type="text" name="id" placeholder="User id" id="id" class="form-control">
        </div>
        <div class="form-group">
            <label for="lastname">Model</label>
            <input type="text" name="model" placeholder="Phone model" id="model" class="form-control">
        </div>
        <button type="submit" class="btn btn-success">Add</button>
    </form>
@endsection
