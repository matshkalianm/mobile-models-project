@extends('layouts.app')

@section('title') user:{{ $usersData->id }} @endsection

@section('content')
    <h1>USER PAGE</h1>
    <table border="1px solid black">
        <tr>
            <th>
                ID
            </th>
            <th>
                First Name
            </th>
            <th>
                Last Name
            </th>
            <th>
                Email
            </th>
            <th>
                Country
            </th>
            <th>
                City
            </th>
            <th>
                Edit
            </th>
            <th>
                Delete
            </th>
        </tr>
        <tr>
            <td>
                {{ $usersData->id }}
            </td>
            <td>
                {{ $usersData->firstname }}
            </td>
            <td>
                {{ $usersData->lastname }}
            </td>
            <td>
                {{ $usersData->email }}
            </td>
            <td>
                {{ $usersData->country }}
            </td>
            <td>
                {{ $usersData->city }}
            </td>
            <td>
                <form action="{{ route('edit.user', $usersData->id) }}" method="post">
                    <input class="btn btn-warning" type="submit" value="Edit"/>
                    @method('get')
                    @csrf
                </form>
            </td>
            <td>
                <form action="{{ route('delete.user', $usersData->id) }}" method="post">
                    <input class="btn btn-danger" type="submit" value="Delete" />
                    @method('delete')
                    @csrf
                </form>
            </td>
        </tr>
    </table>
@endsection
