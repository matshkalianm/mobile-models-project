@extends('layouts.app')

@section('title') Home @endsection

@section('content')
    <h1>HOME PAGE</h1>
    <p>Before we dive into the examples, I think it's worth reviewing the elements of a brilliant homepage design.
        For reference, we covered the following in greater detail in our first post here, but I think it's worth a quick refresher.
        Obviously "brilliant" is largely subjective, but in general, here are some qualities that indicate a brilliant homepage ...

        Clearly answers "Who I am," "What I do," and/or "What you (the visitor) can do here."
        Resonates with the target audience.
        Features a compelling value-proposition.
        Offers great navigation, usability, and mobility.
        Includes calls-to-action (CTAs) to guide visitors to the next logical step.
        Is always changing to reflect the needs, problems, and questions of its visitors.
        (This is why some of the examples featured below and in our downloadable collection may no longer be live!)
        Has a great overall look and feel.
        While some of the homepages featured below may not be exemplary of all these elements in one,
        they were chosen to emphasize specific elements from the list above. If you're considering doing a homepage redesign,
        do your research and take a look at other websites for inspiration. In addition to the ones featured below and in our
        downloadable collection,
        there is no shortage of great design examples out there. Remember that your goal should be to create an aesthetically
        pleasing homepage that represents your brand,
        and also provides your visitors with a user-friendly experience. Take the time to learn about your target audience,
        and create a truly wonderful experience around their specific needs.
    </p>
@endsection
@section('aside')
    @parent
    <p>Welcome to our Page</p>
@endsection
