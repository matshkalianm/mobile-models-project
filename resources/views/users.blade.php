@extends('layouts.app')

@section('title') Show Users @endsection

@section('content')
    <h1>USERS PAGE</h1>
    <table border="1px solid black">
        <tr>
            <th>
                ID
            </th>
            <th>
                Firts Name
            </th>
            <th>
                Last Name
            </th>
            <th>
                Email
            </th>
            <th>
                Country
            </th>
            <th>
                City
            </th>
            <th>
                Phones
            </th>
            <th>
                Show
            </th>
        </tr>
        @foreach($usersData as $element)
            <tr>
                <td>
                    {{ $element->id }}
                </td>
                <td>
                    {{ $element->firstname }}
                </td>
                <td>
                    {{ $element->lastname }}
                </td>
                <td>
                    {{ $element->email }}
                </td>
                <td>
                    {{ $element->country }}
                </td>
                <td>
                    {{ $element->city }}
                </td>
                <td>
                    <a href="{{ route('phones.list', $element->id) }}"> <button class="btn btn-info">Phones</button></a>
                </td>
                <td>
                    <a href="{{ route('one.user', $element->id) }}"> <button class="btn btn-primary">Show</button></a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
