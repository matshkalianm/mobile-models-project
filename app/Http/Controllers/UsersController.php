<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRequest\CreateUserRequest;
use App\Models\Users;
use App\Http\Requests\UsersRequest\UpdateUserRequest;

class UsersController extends Controller
{
    public function addUser(CreateUserRequest $request)
    {
        $user = Users::create
        (
            $request->getFirstName(),
            $request->getLastName(),
            $request->getEmail(),
            $request->getCountry(),
            $request->getCity()
        );

        $user->save();
        return redirect()->route ('users')->with ('success', 'User registered successfully');
    }
    public function saveUsersUpdating($id, UpdateUserRequest $request)
    {
        $user=Users::find($id);
        $user->saveUsersUpdating
        (
            $request->getFirstName(),
            $request->getLastName(),
            $request->getEmail(),
            $request->getCountry(),
            $request->getCity()
        );

        $user->save();

        return redirect()->route ('one.user', $id)->with ('success', 'User info updated successfully');
    }
    public function deleteUser ($id)
    {
        $user=Users::find($id);
        $user->deleteUser();

        return redirect()->route ('users')->with ('success', 'User deleted successfully');
    }
    public function allUsers()
    {
        return view
        (
            'users',
            [
                'usersData' => Users::all()
            ]
        );
    }
    public function showOneUser($id)
    {
        return view
        (
            'oneUser',
            [
                'usersData' => Users::find($id)
            ]
        );
    }
    public function editUser($id)
    {
        return view
        (
            'editUser',
            [
                'usersData' => Users::find($id)
            ]
        );
    }
}
