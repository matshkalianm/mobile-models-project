<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserPhonesRequest;
use App\Http\Requests\PhoneRequest\CreatePhoneRequest;
use App\Http\Requests\PhoneRequest\UpdatePhoneRequest;
use App\Models\Phones;
use App\Models\Users;


class PhonesController extends Controller
{
    public function create (CreatePhoneRequest $request)
    {
        $phone = Phones::create
        (
            $request->getUserId(),
            $request->getModel()
        );

        $phone->save();

        return redirect()->route('add.phone');
    }
    public function savePhoneUpdating($id, UpdatePhoneRequest $request)
    {
        $phone = Phones::find($id);
        $phone->savePhoneUpdating
        (
            $request->getUserId(),
            $request->getModel(),
        );
        $phone->save();
        return redirect()->route('users')->with('success', 'Phones list updated successfully');
    }
    public function deletePhone ($id)
    {
        $phone = Phones::find($id);
        $phone->deletePhone();
        return redirect()->route('users');
    }
    public function allPhones(UserPhonesRequest $request)
    {
        $id = $request->route('id');
        $user = Users::find($id);
        $userPhones = $user->phones;
        return view(

            'phonesList',
            [
                'phonesdata' => $userPhones,
            ]
        );
    }
    public function add()
    {
        return view(
            'addPhones'
        );
    }
    public function editPhone($id)
    {
        return view(
            'editPhones',
            [
                'phones' => Phones::find($id),
            ]
        );
    }
}
