<?php

namespace App\Http\Requests\PhoneRequest;


use Illuminate\Foundation\Http\FormRequest;

abstract class PhonesRequest extends FormRequest
{
    const ID = 'id';
    const MODEL = 'model';

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            self::ID => 'required',
            self::MODEL => 'required',
        ];
    }
    public function getUserId()
    {
        return $this->input(self::ID);
    }
    public function getModel()
    {
        return $this->input(self::MODEL);
    }
}
