<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserPhonesRequest extends FormRequest
{
    const ID = 'id';
    const MODEL = 'model';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

        ];
    }
    public function getUserId()
    {
        return $this->input(self::ID);
    }
    public function getModel()
    {
        return $this->input(self::MODEL);
    }
}
