<?php

namespace App\Http\Requests\UsersRequest;

use Illuminate\Foundation\Http\FormRequest;

abstract class UsersRequests extends FormRequest
{
    const ID = 'id';
    const FIRST_NAME = 'firstname';
    const LAST_NAME = 'lastname';
    const EMAIL = 'email';
    const COUNTRY = 'country';
    const CITY = 'city';

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return
        [
            self::FIRST_NAME => 'required|string|min:3|max:15',
            self::LAST_NAME => 'required|string|min:5|max:15',
            self::EMAIL => 'required|email',
            self::COUNTRY => 'required|string|min:3|max:15',
            self::CITY => 'required|string|min:3|max:15'
        ];
    }
    public function getID() :int{
        return $this->id;
    }
    public function getFirstName(): string
    {
        return $this->input(self::FIRST_NAME);
    }
    public function getLastName():string
    {
        return $this->input(self::LAST_NAME);
    }
    public function getEmail():string
    {
        return $this->input(self::EMAIL);
    }
    public function getCountry():string
    {
        return $this->input(self::COUNTRY);
    }
    public function getCity():string
    {
        return $this->input(self::CITY);
    }
}
