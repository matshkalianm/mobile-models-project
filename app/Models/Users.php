<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Users
 *
 * @package App\Models
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $country
 * @property string $city
 * @property-write phones
 * @property-write create
 * @property-write saveUsersUpdating
 * @property-write deleteUser
 */

class Users extends Model
{
    use HasFactory;

    public function phones(): HasMany
    {
        return $this->hasMany(Phones::class);
    }

    public function setFirstName(string $firstName){
        $this->firstname=$firstName;
    }
    public function getFirstName($firstName){
        return $this->firstname=$firstName;
    }
    public function setLastName(string $lastName){
        $this->lastname=$lastName;
    }
    public function getLastName($lastName){
        return $this->lastname=$lastName;
    }
    public function setEmail(string $email){
        $this->email=$email;
    }
    public function getEmail($email){
        return $this->email=$email;
    }
    public function setCountry(string $country){
        $this->country=$country;
    }
    public function getCountry($country){
        return $this->country=$country;
    }
    public function setCity(string $city){
        $this->city=$city;
    }
    public function getCity($city){
        return $this->city=$city;
    }
    public static function create
        (
            $firstName,
            $lastName,
            $email,
            $country,
            $city
        )
    {
        $user = new self();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setEmail($email);
        $user->setCountry($country);
        $user->setCity($city);
        return $user;

    }
    public  function saveUsersUpdating
    (
        $firstName,
        $lastName,
        $email,
        $country,
        $city
    )
    {
        $this->getFirstName($firstName);
        $this->getLastName($lastName);
        $this->getEmail($email);
        $this->getCountry($country);
        $this->getCity($city);

        return $this;
    }
    public function deleteUser(){
        $this->delete();
    }
}
