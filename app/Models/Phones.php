<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Phones
 *
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property string $model
 * @property-write savePhoneUpdating
 * @property-write create
 * @property-write deletePhone
 */

class Phones extends Model
{
    use HasFactory;

    public function setUserId(int $userId){
        $this->users_id=$userId;
    }
    public function getUserId($userId){
        return $this->users_id=$userId;
    }
    public function setModel(string $model){
        $this->model=$model;
    }
    public function getModel($model){
        return $this->model=$model;
    }

    public static function create
    (
        $userId,
        $model
    )
    {
        $phone = new self();
        $phone->setUserId($userId);
        $phone->setModel($model);

        return $phone;
    }
    public  function savePhoneUpdating
    (
        $userId,
        $model
    )
    {
        $this->getUserId($userId);
        $this->getModel($model);

        return $this;
    }

    public function deletePhone(){
        $this->delete();
    }
}

