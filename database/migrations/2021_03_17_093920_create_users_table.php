<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email'); // TODO make email unique
            $table->string('country');
            $table->string('city');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
